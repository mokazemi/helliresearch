# This Is Nothing More Than a Planets Simulation!
# By: MoKzemi 
# mohammad-kazemi@srbiau.ac.ir
# Published under Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) 
import pygame
from math import *

height=800
width=800

dt=0.1
g=(0,0)

m1=100000.0
pos1=(400,400)
r1=30
v1=(0.0,0.0)

m2=1000.0
pos2=(300,500)
r2=20
v2=(30.0,10.0)

G=1
def distance(p1,p2):
	return sqrt( (p1.pos[0]-p2.pos[0])**2 +(p1.pos[1]-p2.pos[1])**2 )
	
def gravity(thePlanet,aPlanet):
	r=distance(p1,p2)
	thePlanet.f=( 
	              (G * thePlanet.m * aPlanet.m/r**2 ) * (aPlanet.pos[0]-thePlanet.pos[0])/r ,
	              (G * thePlanet.m * aPlanet.m/r**2 ) * (aPlanet.pos[1]-thePlanet.pos[1])/r
	              )
	return thePlanet.f

class planet:
	def __init__(self,m,a,pos,v,r):
		self.m=m
		self.a=a
		self.pos=pos
		self.v=v
		self.r=r
		self.f=(0,0)
		self.int_pos=(int(self.pos[0]), int(self.pos[1]))
	
	def next(self):
		#self.f=gravity(p1,p2)
		self.a=(self.f[0]/self.m , self.f[1]/self.m)
		self.v=(self.v[0]+self.a[0]*dt , self.v[1]+self.a[1]*dt)
		self.pos=(self.pos[0]+self.v[0]*dt , self.pos[1]+self.v[1]*dt)
		self.int_pos=(int(self.pos[0]), int(self.pos[1]))
		self.f=(0,0)



pygame.init()
clock = pygame.time.Clock()
screen=pygame.display.set_mode((width,height))


p1=planet(m1,(0,0),pos1,v1,r1)
p2=planet(m2,(0,0),pos2,v2,r2)

pygame.display.flip()

while True:
	msElapsed = clock.tick(100)
	screen.fill((0,0,0))

	
	pygame.draw.circle(screen,(231,124,33,255),p1.int_pos,p1.r)
	pygame.draw.circle(screen,(123,12,44,255),p2.int_pos,p2.r)
	print(gravity(p1,p2))
	print(gravity(p2,p1))
	p1.next()
	p2.next()
	pygame.display.update()

	for event in pygame.event.get():
		if event.type==pygame.QUIT:
			exit()

